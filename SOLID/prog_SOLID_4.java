package test;
interface vehicle
{
	void showspeed();
}
class car implements vehicle
{
	int speed=0;
	String name="";
	car(int speed,String name)
	{this.speed=speed;this.name=name;}
	@Override
	public void showspeed() {
		// TODO Auto-generated method stub
		System.out.println(this.speed);
	}
	void opendoors()
	{System.out.println("Door Opened");}
}
class bike implements vehicle
{
	int speed=0;
	bike(int speed)
	{this.speed=speed;}
	@Override
	public void showspeed() {
		// TODO Auto-generated method stub
		System.out.println(this.speed);
	}
}
public class prog_SOLID_4 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		bike a=new bike(10);
		car b=new car(15,"Suzuki");
		a.showspeed();
		b.showspeed();
		b.opendoors();
	}

}
